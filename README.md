```bash
Usage: 
  ./crypto_des.sh <Input> <Output> [Options] 

Options: 
  [-P <password>]   Set password, or read from stdin later 
  [-e | -d]         Set Encrypt OR Decrypt mode, guess if neither "-e" nor "-d" is set 

Guess Mode: 
  Grep the suffix ".des3" in <Input> and <Output>
```