#! /bin/bash
#
# note "file crypto" related file
# Copyright (C) 2023 Dramalife@live.com
# 
# This file is part of [note](https://github.com/Dramalife/note.git)
# 
# note is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#

set -e

find . -name "*.case.sh" | while read i ; do 
  echo "Test $i ..."
  if bash $i >/dev/null 2>&1 ; then
    echo "  SUCCESS"
  else
    echo "  FAILED!"
  fi
done
