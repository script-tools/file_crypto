PASSWD=123456
FNAME_DATA=data
CRYPTO=../crypto_des.sh
TEMP_DIR=/tmp/.test_crypto_file_dramalife
D0_PLAIN=./dir1/$FNAME_DATA
D0_CIPHER=./dir1.des3

D0_ENC=$TEMP_DIR/d0.des3
D0_DEC=$TEMP_DIR/d0_dec

mkdir -p $TEMP_DIR
mkdir -p $D0_DEC

t_find_data(){
  find $1 -name $FNAME_DATA
}
