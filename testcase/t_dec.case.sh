#! /bin/bash
#
# note "file crypto" related file
# Copyright (C) 2023 Dramalife@live.com
# 
# This file is part of [note](https://github.com/Dramalife/note.git)
# 
# note is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#

set -e
set -x
source common.sh

$CRYPTO $D0_CIPHER $D0_DEC -P $PASSWD
diff $(t_find_data $D0_PLAIN) $(t_find_data $D0_DEC)

source clean.sh
